# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.7.1] - 2021-11-29
### Changed
- Add `generateExcelDownloadWithMultipleSheets` to allow generating multiple sheets in excel

## [1.6.24] - 2021-08-02
### Changed
- Fixed `ChipField` 'disabled' props

## [1.6.23] - 2021-08-02
### Changed
- Add `hideDelete` to `UploadFieldImage` component in `MediaField`

## [1.6.22] - 2021-07-18
### Changed
- Changed `TextField` util import to prevent downstream apps having to download extraneous util deps e.g. xlsx

## [1.6.21] - 2021-04-15
### Changed
- Fixed `getGraphQLFilter` to allow `between` op values

## [1.6.20] - 2021-03-27
### Changed
- Changed `DataTable` to fallback to empty array if missing items

## [1.6.19] - 2021-03-26
### Changed
- Fixed `getGraphQLFilter` to accept booleans as value

## [1.6.18] - 2021-03-14
### Changed
- Added `role` attribute to `DataTable`
## [1.6.17] - 2021-03-01
### Changed
- Changed `ArrayField` to add `isDefaultEmpty` prop to allow empty array by default

## [1.6.16] - 2021-03-01
### Changed
- Changed `ChipField` to fix typing

## [1.6.15] - 2021-03-01
### Changed
- Changed `ChipField` to allow `options` prop

## [1.6.14] - 2021-02-27
### Changed
- Changed typing of `omitTypename` response

## [1.6.13] - 2021-02-27
### Changed
- Changed render of the first column in DataTable to avoid wrapper children with h6 by default

## [1.6.12] - 2021-02-16
### Added
- Added `renderRow` prop to DataTable to accept custom rows

## [1.6.11] - 2021-02-01
### Added
- Add `cardHeader` prop to DataTable to allow overriding of default card header

## [1.6.10] - 2021-01-28
### Changed
- Make npm package public

## [1.6.9] - 2021-01-27
### Changed
- Patch TextField to allow falsy option values

## [1.6.8] - 2021-01-15
### Changed
- Patch `isHideCheckbox` prop in DataTable to hide both cells in table header and table rows

## [1.6.7] - 2021-01-15
### Changed
- Add `isHideCheckbox` prop to DataTable

## [1.6.6] - 2020-12-11
### Changed
- Add `hideDelete` prop to MediaField

## [1.6.5] - 2020-12-10
### Changed
- Changed `usePagination` to prevent duplicate records for cache-and-network fetch policy

## [1.6.4] - 2020-12-07
### Changed
- Changed `S3Image` to ensure that loading state is only shown when width and height are set

## [1.6.3] - 2020-12-07
### Changed
- Changed `TextField` to ensure that `errors` prop is typed as optional

## [1.6.1] - 2020-12-06
### Changed
- Changed `S3Image` to show loading state when the `src` prop is not yet available

## [1.6.0] - 2020-11-29
### Changed
- Changed `TextField` to ensure `error` prop is cast as Boolean to resolve type issues
- Minor upgrade due to React.forwardRef change from before 

## [1.5.26] - 2020-11-29
### Changed
- Changed `TextField` to forwardRef instead due to this current issue on RHF: https://github.com/react-hook-form/react-hook-form/issues/3411

## [1.5.25] - 2020-11-29
### Changed
- Changed `TextField` to pass injected `ref` into MUI inputRef prop and sugar prop `errors` handling error state

## [1.5.24] - 2020-11-29
### Changed
- Changed `TextField` to include aria-label

## [1.5.23] - 2020-11-09
### Changed
- Fixed `MediaField` to set mimetype on s3 upload

## [1.5.22] - 2020-10-14
### Changed
- Changed `generateExcelDownload` to allow merging of cells using merge param

## [1.5.21] - 2020-10-12
### Changed
- Added `S3Video`
- Added `Loading`
- Changed `MediaField` to allow video uploads

## [1.5.20] - 2020-9-18
### Changed
- Changed `MoneyField` to include `label`

## [1.5.19] - 2020-09-14
### Added
- Added `option.disabled` check to disable option in `TextField`

## [1.5.18] - 2020-09-11
### Changed
- Added `readOptions` prop to `ExcelUploadForm`

## [1.5.17] - 2020-09-08
### Changed
- Changed `TextField` to accept 'title' as label key when in select mode

## [1.5.2] - 2020-06-08
### Changed
- Changed `validateExcelData` data response shape and prevent it from aborting early so as to get all available errors instead of just the first error

## [1.5.1] - 2020-06-07
### Added
- Added `yup` to devDeps and peerDeps 

## [1.5.0] - 2020-06-07
### Added
- Added `ExcelUploadForm`, `DropzoneCard` to core for managing excel uploads
- Added `StepContentBox` to core for managing Stepper content
- Added `useStepper` to hooks for managing stepper navigation state
- Added `generateExcelDownload` to utils for writing excel downloads
- Added `validateExcelData` to utils for validating objects against a yup schema
- Added `getTitleToIDObject` to utils for transforming data
- Added `xlsx` package to peerDeps 

### Changed
- Changed `moreActions` prop (object[]) to DataTable for managing more button actions in the DataTable

## [1.3.5] - 2020-05-29
### Changed
- Fix `getGraphQLConnectionFieldValue` to fallback to id if pk is missing in the value provided so as to return option selected.

## [1.0.1] - 2020-05-17
### Added
- Added `PaginationProps` interface to hooks/usePagination

## [1.0.0] - 2020-05-17
### Added
- Hooks folder
- hooks/usePagination
- This CHANGELOG.md file
- @apollo/react-hooks package to devDeps for typing

### Changed
- Updated README.md with instructions on installation, tree-shaking and yarn link 

### Removed
- Grouping of Utils. Utils are now to be written as singleton files with default exports
- utils/Filters in favour of hooks/useFilters, utils/getFilterChips, and utils/getGraphQLFilter 
- utils/Sort in favour of hooks/useSort
- utils/Form in favour of utils/getGraphQLConnectionFieldValue, utils/cleanFieldArray, utils/isEmptyFieldArray, utils/getLabelFromName
