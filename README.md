# GVS-KIT
> Re-usable modules for GVS applications

## Installation
```
yarn add @onextech/gvs-kit
```

## Usage

```
import { TextField } from '@onextech/gvs-kit'
```

## Tree-shaking (Recommended)

To minimze bundle size of this package, do the following:

1. In project, run `yarn add -D babel-plugin-transform-imports` 
2. Add the following to your `babel.config.js`
```
// babel.config.js
module.exports = {
    ...
    plugins: [
        ...
        '@onextech/gvs-kit/core': {
          transform: '@onextech/gvs-kit/core/${member}',
          preventFullImport: true,
        },
        '@onextech/gvs-kit/utils': {
          transform: '@onextech/gvs-kit/utils/${member}',
          preventFullImport: true,
        },
        '@onextech/gvs-kit/hooks': {
          transform: '@onextech/gvs-kit/hooks/${member}',
          preventFullImport: true,
        },
    ]
}
```

References:
- https://material-ui.com/guides/minimizing-bundle-size

## Deployment Steps
I. Update `CHANGELOG.md`

II. Commit your changes
```
git add . && git commit -m "<MSG>" && git push
```
III. Version
```
yarn version --[patch/minor/major]
```
IV. Build
```
yarn build
```
V. Publish
```
npm publish ./dist
```
VI. Push
```
git push
```

## How to work with GVS-KIT locally
To preview changes to gvs-kit in your project without publishing the project, you will need to link the project locally using `yarn link`. See below for instructions on linking and unlinking this library to your project locally.

References:
- About [yarn link](https://classic.yarnpkg.com/en/docs/cli/link)
- Note: We have to link React because [hooks don't work with yarn link](https://github.com/facebook/react/issues/14257#issuecomment-595183610)

Pre-requisites:
- NextJS >= 9.4
- babel-plugin-transform-imports >= 2.x

### Linking

1. In gvs-kit, run `yarn link`
2. In gvs-kit, run `cd node_modules/react` and run `yarn link`
3. In gvs-kit, run `cd node_modules/react-dom` and run `yarn link`
4. In gvs-kit, run `cd node_modules/@material-ui/core` and run `yarn link`
5. In gvs-kit, run `cd node_modules/aws-amplify` and run `yarn link`
6. In project, run `yarn link @onextech/gvs-kit react react-dom @material-ui/core aws-amplify`
7. In project's `babel.config.js`, make edits so that the entries look like this:
```
// babel.config.js
module.exports = {
    ...
    plugins: [
        ...
        '@onextech/gvs-kit/core': {
          transform: '@onextech/gvs-kit/dist/core/${member}', // TODO: Remove /dist when done
          preventFullImport: true,
        },
        '@onextech/gvs-kit/utils': {
          transform: '@onextech/gvs-kit/dist/utils/${member}', // TODO: Remove /dist when done
          preventFullImport: true,
        },
        '@onextech/gvs-kit/hooks': {
          transform: '@onextech/gvs-kit/dist/hooks/${member}', // TODO: Remove /dist when done
          preventFullImport: true,
        },
    ]
}
```

- Finally, in gvs-kit, make changes and then run `yarn build` to see changes reflected in project.

### Unlinking

1. In project, run `yarn unlink @onextech/gvs-kit react react-dom @material-ui/core aws-amplify`
2. In project, run `yarn install --force` to refresh deps
3. In project, revert changes in `babel.config.js`
