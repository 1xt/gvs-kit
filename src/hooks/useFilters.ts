import { useState } from 'react'

interface FilterType {
  [key: string]: any
}

type AddFilter = (newFilter: FilterType) => void

type RemoveFilter = ({ key }: { key: string }) => void

type UseFilters = (initialState?: FilterType | null) => [FilterType | null, AddFilter, RemoveFilter]

const useFilters: UseFilters = (initialState = null) => {
  const [filters, setFilters] = useState<FilterType | null>(initialState)

  const addFilter: AddFilter = (newFilter) => setFilters((filters) => ({ ...filters, ...newFilter }))

  const removeFilter: RemoveFilter = ({ key }) => {
    return setFilters((filters) => {
      const nextFilters = { ...filters }
      delete nextFilters[key]

      // Set on null on empty objects to reset query
      if (Object.keys(nextFilters).length === 0) return null

      return nextFilters
    })
  }

  return [filters, addFilter, removeFilter]
}

export default useFilters
