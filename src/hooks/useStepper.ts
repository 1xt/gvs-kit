import { useState } from 'react'

export interface UseStepperData {
  next: () => void
  back: () => void
  skip: () => void
  reset: () => void
  isStepSkipped: (step: string) => boolean
  activeStep: string
  activeStepIndex: number
}

const useStepper = (steps: string[]): UseStepperData => {
  const initialStep = steps[0]
  const [activeStep, setActiveStep] = useState(initialStep)
  const [skipped, setSkipped] = useState(new Set<string>())

  const getStepIndex = (prevActiveStep: string) => steps.findIndex((step) => step === prevActiveStep)
  const activeStepIndex = getStepIndex(activeStep)
  const isStepSkipped = (step: string) => skipped.has(step)
  const handleNext = () => {
    let newSkipped = skipped

    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values())
      newSkipped.delete(activeStep)
    }

    setActiveStep((prevActiveStep) => steps[getStepIndex(prevActiveStep) + 1])
    setSkipped(newSkipped)
  }
  const handleBack = () => setActiveStep((prevActiveStep) => steps[getStepIndex(prevActiveStep) - 1])
  const handleSkip = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values())
      newSkipped.add(activeStep)
      return newSkipped
    })
  }
  const handleReset = () => setActiveStep(initialStep)

  return {
    next: handleNext,
    back: handleBack,
    skip: handleSkip,
    reset: handleReset,
    isStepSkipped,
    activeStep,
    activeStepIndex,
  }
}

export default useStepper
