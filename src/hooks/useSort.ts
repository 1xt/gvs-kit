import { useState } from 'react'

// Pass in the generated Searchable*SortInput for generic T
const useSort = <T = any>(initialState: T | null = null) => {
  return useState<T | null>(initialState)
}

export default useSort
