import { useState } from 'react'
import { QueryHookOptions } from '@apollo/react-hooks'
import { QueryResult } from '@apollo/react-common'
import { ApolloQueryResult } from 'apollo-client'

const getLoadMore = (fetchMore: QueryResult['fetchMore'], options: QueryHookOptions) => () => {
  return fetchMore({
    ...options, // Ensure that we pass in the latest nextToken into the options.variables here to query the new result
    updateQuery: (previousResult, { fetchMoreResult }) => {
      // Relies on apollo data having only 1 enumerable key
      const getQueryName = (result) => result && Object.keys(result)[0]
      const queryName = getQueryName(fetchMoreResult)
      const prev = previousResult[queryName]
      const next = fetchMoreResult[queryName]
      const isSamePage = prev.nextToken === next.nextToken
      const isLastPage = prev.nextToken === null
      const shouldAppendPage = !(isSamePage || isLastPage)
      const nextItems = shouldAppendPage ? [...prev.items, ...next.items] : prev.items
      const nextResult = {
        [queryName]: {
          ...next,
          items: nextItems,
          nextToken: next.nextToken,
        },
      }
      return nextResult
    },
  })
}
export interface PaginationProps {
  loadMore: () => Promise<ApolloQueryResult<any>> | null
  hasMore: boolean
}
/**
 *
 * @example
 * const pagination = usePagination(nextToken, fetchMore, {
 *   query,
 *   variables,
 *   skip: loading,
 * })
 *
 * @param nextToken
 * @param fetchMore
 * @param options
 */
const usePagination = (
  nextToken: string,
  fetchMore: QueryResult['fetchMore'],
  options: QueryHookOptions
): PaginationProps => {
  const { skip, variables } = options
  const [prevNextToken, setPrevNextToken] = useState<string | null>(null)
  // Ensure that `loadMore` does not errorneously fire queries
  if (skip) return { loadMore: () => null, hasMore: false }
  // Pass in nextToken into variables to ensure that query the next page
  const nextVariables = { ...variables, nextToken }
  const loadMore = () => {
    // Prevents duplicate records for cache-and-network fetch policy; refer to: https://github.com/apollographql/apollo-client/issues/5901
    const isSameToken = prevNextToken === nextToken
    if (isSameToken) return null
    setPrevNextToken(nextToken)
    return getLoadMore(fetchMore, { ...options, variables: nextVariables })()
  }
  const hasMore = Boolean(nextToken)
  return { loadMore, hasMore }
}
export default usePagination
