import { isEmpty } from 'lodash'

interface FilterType {
  [key: string]: any
}

interface FilterOp {
  eq?: string | number | boolean
  ne?: string | number | boolean
  lt?: string | number
  le?: string | number
  gt?: string | number
  ge?: string | number
  contains?: string | number
  notContains?: string | number
  betweenContains?: string | number
}

type FilterField = {
  op: string
  key?: string
  filterKey?: string // Override GraphQL filter key
}

interface FilterFields {
  [key: string]: FilterField
}

interface OrFilterInput {
  [name: string]: { [op in keyof FilterOp]: FilterOp[keyof FilterOp] }
}

interface OrFilter {
  or: OrFilterInput[]
}

interface GraphQLFilterOutput {
  and: OrFilter[]
}

/**
 * Define objectID by Key or ID
 *
 * Extracts an ID from an object like { categoryID: 'Sjk2hA827Al2918' }
 * based on a key like 'category'
 *
 * Return a primitive that represents thhe object ID: e.g. 'Sjk2hA827Al2918'
 *
 * @param obj
 * @param key
 */
const getObjectID = (obj: any, key: string): string => {
  const valueByKey = obj[key]
  const keysEndingWithID = Object.keys(obj).filter((k) => k.endsWith('ID'))
  const valueByID = obj[keysEndingWithID[0]]
  return valueByKey || valueByID
}

/**
 * Computes filter op value based on op and value type
 *
 * @param key
 * @param value
 * @param op
 */
const getFilterOpValue = (key: string, value: any, op: string): string | number => {
  const isObject = typeof value === 'object' && !Array.isArray(value)
  if (isObject) return getObjectID(value, key)

  const isWildCard = op === 'wildcard'
  if (isWildCard) return `${value}*`

  return value
}

/**
 * Extracts the op value across arrays and objects
 * in order to an array of op:value objects
 *
 * @param key
 * @param value
 * @param filterField
 */
const getOrFilterInput = (key: string, value: any, filterField: FilterField): OrFilterInput[] => {
  const { op, filterKey } = filterField
  const nextKey = filterField.key || key
  const isBetweenOp = op === 'between'

  const orValue = (Array.isArray(value) && !isBetweenOp
    ? value.map((val: any) => ({ [op]: getFilterOpValue(nextKey, val, op) }))
    : [{ [op]: getFilterOpValue(nextKey, value, op) }]
  ).map((nextValue) => ({ [filterKey || nextKey]: nextValue }))

  return orValue
}

/**
 * getGraphQLFilter
 *
 * This function converts simple key:value pairs into complex AppSync GraphQL Filters
 * that have the ability to do `and/or` chaining.
 *
 * @example
 *
 * const modelFilterFields = {
 *   statuses: { op: 'eq', key: 'status' },
 *   categories: { op: 'eq', key: 'categoryID' },
 *   search: [
 *     { key: 'title', value: { op: 'wildcard' } },
 *     { key: 'subtitle', value: { op: 'wildcard' } },
 *   ],
 * }
 *
 * const filters = {
 *   statuses: ['published'],
 *   categories: [{ id: 'Sjk2hA827Al2918', title: 'Chocolate' }],
 *   search: 'The'
 * }
 *
 * const getProductFilter = getGraphQLFilter(modelFilterFields)
 * const productFilter = getProductFilter(filters)
 *
 * // productFilter
 * {
 *   and: [
 *     { or: [{ status: { eq: 'published' } }] },
 *     { or: [{ categoryID: { eq: 'Sjk2hA827Al2918' } }] },
 *     { or: [{ title: { wildcard: 'The*' } }, { subtitle: { wildcard: 'The*' } }] },
 *   ]
 * }
 *
 */
const getGraphQLFilter = <T = FilterFields>(filterFields: T) => (filters: FilterType): GraphQLFilterOutput | null => {
  if (!filters) return null

  const orFilters = Object.entries(filters).reduce<OrFilter[]>((acc, filter) => {
    const [key, value] = filter

    const filterOption = filterFields[key]
    if (!filterOption || (typeof value === 'object' && isEmpty(value))) return acc

    if (Array.isArray(filterOption)) {
      const orValues = filterOption.reduce((acc, { key: filterFieldKey, value: filterField }) => {
        const orValue = getOrFilterInput(filterFieldKey, value, filterField)
        return acc.concat(orValue)
      }, [])
      const filterInput = { or: orValues }
      return acc.concat(filterInput)
    }

    const orValue = getOrFilterInput(key, value, filterOption)
    const filterInput = { or: orValue }

    return acc.concat(filterInput)
  }, [])

  return orFilters.length ? { and: orFilters } : null
}

export default getGraphQLFilter
