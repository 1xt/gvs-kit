import omitDeep from './omitDeep'

function omitTypename<T = Record<string, unknown>>(obj): T {
  return omitDeep(obj, ['__typename'])
}

export default omitTypename
