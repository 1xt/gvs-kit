import { intersectionWith } from 'lodash'

export interface OptionType {
  title: string
  [pk: string]: any
}

/**
 * getGraphQLConnectionFieldValue
 *
 * Get field value based on a GraphQL connection
 * Apply to form field values of a pair of connected models
 * Normalises the value based on the join table
 * Used specifically for AWS Amplify ModelConnections
 * Initially developed to work with MUI Autocomplete.
 *
 * @param pk {string}
 * @param options {object[]}
 * @param value {object | object[]}
 * @param multiple {boolean}
 * @return object | object[] | null
 */
const getGraphQLConnectionFieldValue = (
  pk: string,
  options: OptionType[],
  value: object | object[],
  multiple = false
): object | object[] => {
  if (!options || !value) return multiple ? [] : value ?? null
  const getObjectValue = (options, value) =>
    options.filter((option) => option[pk] === value[pk] || option[pk] === value.id)[0]
  const getArrayValue = (options, value) => intersectionWith(options, value, (a, b) => a[pk] === (b[pk] || b.id))
  const getValue = multiple ? getArrayValue : getObjectValue
  return getValue(options, value) ?? null
}

export default getGraphQLConnectionFieldValue
