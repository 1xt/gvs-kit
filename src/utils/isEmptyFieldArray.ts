import cleanFieldArray from './cleanFieldArray'

const isEmptyFieldArray = (arr) => (arr ? cleanFieldArray(arr).length === 0 : true)

export default isEmptyFieldArray
