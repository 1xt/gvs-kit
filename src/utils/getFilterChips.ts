import { startCase, omit } from 'lodash'

export interface FilterType {
  [key: string]: any
}

export interface FilterChip {
  key: string
  name: string
  label: string
}

const getFilterChipValue = (key: string, value: any) => {
  switch (true) {
    case Array.isArray(value):
      return value
        .map((val: any) => {
          const isObj = typeof val === 'object'
          const nextVal = isObj ? val['title' || 'name'] : val
          return nextVal
        })
        .join(', ')
    case value === null:
      return value
    case typeof value === 'object':
      return value['title' || 'name']
    default:
      return value
  }
}

interface GetFilterChipOptions {
  hide?: string[]
}

/**
 * getFilterChips
 *
 * Set filter chip data to render
 *
 * @example
 * const filterChips = getFiltersChips(filters)
 *
 * // filterChips
 * [{
 *   key: 'status',
 *   value: 'status',
 *   label: 'Status: Published'
 * }]
 *
 * @param filters
 * @param options
 */
const getFilterChips = (filters: FilterType | null, options: GetFilterChipOptions = {}): FilterChip[] => {
  if (!filters) return []

  // Handle hide
  const { hide } = options
  const nextFilters = hide ? omit(filters, hide) : filters

  return Object.entries(nextFilters).reduce<FilterChip[]>((acc, [key, value]) => {
    const nextValue = getFilterChipValue(key, value)
    if (!nextValue) return acc

    return acc.concat({
      key,
      name: startCase(key),
      label: nextValue,
    })
  }, [])
}

export default getFilterChips
