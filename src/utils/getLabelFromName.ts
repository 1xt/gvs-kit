import { startCase } from 'lodash'

const getLabelFromName = (name: string): string => {
  const nextName = name.includes('.') ? name.split('.').pop() : name
  return startCase(nextName)
}

export default getLabelFromName
