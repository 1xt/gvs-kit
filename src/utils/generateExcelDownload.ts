import XLSX from 'xlsx'

const generateExcelDownload = (excelDataRowHeader, filename, merge = []) => {
  /* make the worksheet */
  const ws = XLSX.utils.json_to_sheet(excelDataRowHeader)
  const nextWs = merge.length ? { ...ws, '!merges': merge } : ws
  /* add to workbook */
  const workbook = XLSX.utils.book_new()
  XLSX.utils.book_append_sheet(workbook, nextWs, 'Sheet1')
  /* generate an XLSX file */
  XLSX.writeFile(workbook, filename, { type: 'file' })
}

export const generateExcelDownloadWithMultipleSheets = (sheets, filename) => {
  /* make the workbook */
  const workbook = XLSX.utils.book_new()

  sheets.forEach(({ rows, merge = [], name }, index) => {
    /* make the worksheet */
    const ws = XLSX.utils.json_to_sheet(rows)
    const nextWs = merge.length ? { ...ws, '!merges': merge } : ws
    /* add the worksheet to workbook */
    XLSX.utils.book_append_sheet(workbook, nextWs, name || `Sheet${index + 1}`)
  })

  /* generate an XLSX file */
  XLSX.writeFile(workbook, filename, { type: 'file' })
}

export default generateExcelDownload
