const isEmptyObject = (obj) => Object.keys(obj).length === 0

const isEmptyObjectValues = (obj) => Object.values(obj).every((val) => typeof val === 'undefined')

const cleanFieldArray = (arr) => arr.filter((obj) => !isEmptyObject(obj) && !isEmptyObjectValues(obj))

export default cleanFieldArray
