export interface TitleToIDObject {
  [title: string]: string
}

const getTitleToIDObject = (records: Array<{ title: string; id: string }>): TitleToIDObject => {
  return records.reduce((acc, record) => {
    const { title, id } = record
    return { ...acc, [title]: id }
  }, {})
}

export default getTitleToIDObject
