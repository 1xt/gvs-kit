import { isEmpty } from 'lodash'

interface FilterType {
  [key: string]: any
}

const getFilterCount = (filters: FilterType): number | null => {
  if (!filters) return null
  return filters && Object.values(filters).filter((x) => !isEmpty(x)).length
}

export default getFilterCount
