const addTypename = (data, typename) => (data ? { ...data, __typename: typename } : null)

export default addTypename
