type ExcludesType = Array<number | string>

/**
 * Recursively remove keys from an object
 * @usage
 *
 * const input = {
 *   id: 1,
 *   __typename: '123',
 *   createdAt: '1020209',
 *   address: {
 *     id: 1,
 *     __typename: '123',
 *   },
 *   variants: [
 *     20,
 *     {
 *       id: 22,
 *       title: 'hello world',
 *       __typename: '123',
 *       createdAt: '1020209',
 *       variantOption: {
 *         id: 1,
 *         __typename: '123',
 *       },
 *     },
 *     {
 *       id: 32,
 *       __typename: '123',
 *       createdAt: '1020209',
 *     },
 *   ],
 * }
 *
 * const output = {
 *   id: 1,
 *   address: {
 *     id: 1,
 *   },
 *   variants: [
 *     20,
 *     {
 *       id: 22,
 *       title: 'hello world',
 *       variantOption: {
 *         id: 1,
 *       },
 *     },
 *     {
 *       id: 32,
 *     },
 *   ],
 * }
 *
 * expect(omitDeep(input, ['createdAt, 'updatedAt', __typename']).to.deep.equal(output) // true
 *
 * @param {object} input
 * @param {Array<number | string>>} excludes
 * @return {object}
 */
function omitDeep<T = object>(input: T | T[], excludes: ExcludesType): T | T[] {
  if (!input) return input

  const omitFromObjectRecursively = (obj: T, excludes: ExcludesType): T => {
    return Object.entries(obj).reduce((acc, [key, value]) => {
      const shouldExclude = excludes.includes(key)
      if (shouldExclude) return acc

      if (Array.isArray(value)) {
        acc[key] = value.map((arrItem) => {
          const isObject = typeof arrItem === 'object'
          return isObject ? omitDeep(arrItem, excludes) : arrItem
        })
        return acc
      }

      if (typeof value === 'object') {
        acc[key] = omitDeep(value, excludes)
        return acc
      }

      acc[key] = value

      return acc
    }, {} as T)
  }

  if (Array.isArray(input)) {
    return input.map((inp) => omitFromObjectRecursively(inp, excludes))
  }

  return omitFromObjectRecursively(input, excludes)
}

export default omitDeep
