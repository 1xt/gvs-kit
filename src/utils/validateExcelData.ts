import { Schema } from 'yup'

const validateExcelData = (schema: Schema<any>, rows: any[]) => {
  if (!rows) return

  return rows.map((row) => {
    try {
      schema.validateSync(row, { abortEarly: false })
      return { ...row, isValid: true }
    } catch (err) {
      return { ...row, errorMessage: err.message, errors: err.errors, isValid: false }
    }
  })
}

export default validateExcelData
