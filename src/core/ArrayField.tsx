import React, { useEffect } from 'react'
import clsx from 'clsx'
import { startCase } from 'lodash'
import { Button, Grid, GridProps, IconButton, Theme, Typography } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/DeleteOutlined'
import AddIcon from '@material-ui/icons/Add'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) => ({
  fieldArray: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },
  button: {
    marginTop: theme.spacing(1),
  },
  label: {
    marginBottom: theme.spacing(2),
  },
  header: {
    fontSize: theme.typography.pxToRem(10),
    fontWeight: 600,
    textTransform: 'uppercase',
    letterSpacing: 1,
    opacity: 0.5,
    marginTop: theme.spacing(-1),
    marginBottom: theme.spacing(-1),
    whiteSpace: 'nowrap',
  },
}))

export interface ArrayFieldRenderProps {
  name: string
  label?: string
  item: any
  index: number
}

export interface HeaderColumn {
  name: string
  label?: string
  grid: GridProps
}

export interface ArrayFieldProps {
  name: string
  label?: string
  // No typing provided by react-hook-form
  fieldArray: any
  children: (props: ArrayFieldRenderProps) => JSX.Element
  header?: HeaderColumn[]
  hideLabel?: boolean
  hideAdd?: boolean
  hideRemove?: boolean
  className?: string
  isDefaultEmpty?: boolean
}

const ArrayField: React.FC<ArrayFieldProps> = (props) => {
  const { isDefaultEmpty, className, header, name, label, fieldArray, children, hideLabel, hideAdd, hideRemove } = props
  const { fields, append, remove } = fieldArray
  const classes = useStyles(props)

  useEffect(() => {
    // Always start with one row by default
    if (fields.length === 0 && !isDefaultEmpty) append({})
  }, [fields])

  return (
    <div className={clsx('MuiArrayField-root', className)}>
      {!hideLabel && (
        <Typography variant="h5" className={classes.label}>
          {label || startCase(name)}
        </Typography>
      )}
      <ul className={classes.fieldArray}>
        {header && (
          <li key="header">
            <Grid container spacing={1}>
              <Grid item xs>
                <Grid container spacing={1}>
                  {header?.map((column) => {
                    const { name, label, grid } = column
                    return (
                      <Grid key={name} item {...grid}>
                        <Typography className={classes.header}>{label || startCase(name)}</Typography>
                      </Grid>
                    )
                  })}
                </Grid>
              </Grid>
              {!hideRemove && <Grid item xs={1} />}
            </Grid>
          </li>
        )}
        {fields.map((item, index) => {
          return (
            <li key={item.id}>
              <Grid container spacing={1}>
                <Grid item xs>
                  {children({ name, label, item, index })}
                </Grid>
                {!hideRemove && (
                  <Grid item xs={1}>
                    <IconButton onClick={() => remove(index)}>
                      <DeleteIcon />
                    </IconButton>
                  </Grid>
                )}
              </Grid>
            </li>
          )
        })}
      </ul>
      {!hideAdd && (
        <Button
          size="small"
          color="primary"
          startIcon={<AddIcon />}
          onClick={() => append({})}
          className={fields?.length > 0 || header ? classes.button : ''}
        >
          Add {(label || startCase(name)).slice(0, -1)}
        </Button>
      )}
    </div>
  )
}

export default ArrayField
