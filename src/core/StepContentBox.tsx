import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, Theme } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2, 3, 3),
  },
  content: {
    minHeight: 120,
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actions: ({ isCompleted }: StepContentBoxProps) => {
    return {
      display: 'flex',
      justifyContent: isCompleted ? 'center' : 'space-between',
      marginTop: theme.spacing(2),
    }
  },
}))

interface StepContentBoxProps {
  onNext?: () => void
  onBack?: () => void
  onCompleted?: () => void
  isCompleted?: boolean
  isNextDisabled?: boolean
  isBackDisabled?: boolean
  isNextHidden?: boolean
  isBackHidden?: boolean
}

const StepContentBox: React.FC<StepContentBoxProps> = (props) => {
  const {
    isCompleted,
    children,
    onCompleted,
    onNext,
    onBack,
    isNextDisabled,
    isBackDisabled,
    isNextHidden,
    isBackHidden,
  } = props
  const classes = useStyles(props)

  return (
    <Box className={classes.root}>
      <div className={classes.content}>{children}</div>
      <div className={classes.actions}>
        {isCompleted && onCompleted ? (
          <Button onClick={onCompleted} color="primary">
            Finish
          </Button>
        ) : (
          <>
            <div>
              {onBack && !isBackHidden && (
                <Button onClick={onBack} color="primary" disabled={isBackDisabled}>
                  Back
                </Button>
              )}
            </div>
            <div>
              {onNext && !isNextHidden && (
                <Button onClick={onNext} color="primary" disabled={isNextDisabled}>
                  Next
                </Button>
              )}
            </div>
          </>
        )}
      </div>
    </Box>
  )
}

export default StepContentBox
