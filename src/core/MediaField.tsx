import React, { useState } from 'react'
import { get } from 'lodash'
import clsx from 'clsx'
import { useDropzone, DropzoneProps } from 'react-dropzone'
import {
  Avatar,
  Badge,
  Box,
  Button,
  IconButton,
  Theme,
  Snackbar,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  ListItem,
  List,
  Card,
  CardActionArea,
  Typography,
  FormLabel,
} from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined'
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import * as colors from '@material-ui/core/colors'
import { Storage } from 'aws-amplify'
import S3Image from './S3Image'
import DragProvider from './DragProvider'
import { removeTypename } from '../utils'
import S3Video from './S3Video'
import Loading from './Loading'

const FILE_SIZE_THRESHOLDS = {
  B: 2 ** 0,
  KB: 2 ** 10,
  MB: 2 ** 20,
  GB: 2 ** 30,
  TB: 2 ** 40,
}
const MAX_FILE_SIZE = FILE_SIZE_THRESHOLDS['MB'] * 1

const getFileSizeLimit = (size: number, fractionDigits?: number): string => {
  const threshold = Object.keys(FILE_SIZE_THRESHOLDS).reduce((threshold, key) =>
    size >= FILE_SIZE_THRESHOLDS[key] ? key : threshold
  )
  return `${(size / FILE_SIZE_THRESHOLDS[threshold]).toFixed(fractionDigits)}${threshold}`
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'relative',
  },
  dbImage: {
    width: 54,
    height: 54,
    borderRadius: 4,
  },
  badge: {
    marginRight: theme.spacing(1),
    '&:last-child': {
      marginRight: 0,
    },
  },
  uploadContainer: {
    height: '100%',
    display: 'flex',
  },
  uploadButton: {
    minWidth: theme.spacing(6.75),
    height: theme.spacing(6.75),
  },
  deleteButton: {
    fontSize: '0.9rem',
    padding: theme.spacing(0.125),
    backgroundColor: theme.palette.background.default,
    '&:hover': {
      backgroundColor: colors.red[500],
      color: theme.palette.common.white,
    },
  },
  dragProvider: {
    '& > div': {
      marginRight: theme.spacing(1),
    },
  },
  listItem: {
    listStyle: 'none',
  },
  uploadCard: ({ uploadCardProps = {} }: MediaFieldProps) => ({
    width: '100%',
    minWidth: 320,
    border: `1px dashed ${colors.grey[400]}`,
    boxShadow: 'none',
    '&:hover': {
      borderColor: theme.palette.primary.main,
    },
    '& .MuiCardActionArea-root': {
      display: 'flex',
      padding: uploadCardProps.padding || theme.spacing(2),
    },
  }),
  label: {
    width: 'fit-content',
    position: 'absolute',
    top: -5,
    fontSize: theme.typography.pxToRem(10.8),
    padding: theme.spacing(0, 0.5),
    marginLeft: theme.spacing(1.25),
    backgroundColor: theme.palette.common.white,
  },
}))

const UploadFieldImage = (props) => {
  const { as: As = 'img', onClear, ImageProps = {}, hideDelete, ...rest } = props
  const classes = useStyles(props)

  return (
    <Badge
      {...rest}
      className={classes.badge}
      badgeContent={
        hideDelete ? null : (
          <IconButton aria-label="delete" size="small" onClick={onClear} className={classes.deleteButton}>
            <ClearIcon fontSize="inherit" />
          </IconButton>
        )
      }
    >
      <As {...ImageProps} className={clsx(classes.dbImage, ImageProps.className)} />
    </Badge>
  )
}

const mapFileWithPrefix = (prefix: string) => (acceptedFile) => {
  const { name, size, type } = acceptedFile
  const timestamp = Date.now()
  const format = type.split('/').pop()
  return {
    src: `${prefix}/${size}${timestamp}.${format}`,
    name,
    size,
    type,
    File: acceptedFile,
  }
}

const putS3File = async (acceptedFile) => {
  const { File, ...rest } = acceptedFile
  const { src, type } = rest
  await Storage.put(src, File, { contentType: type })
  return rest
}

const mapItemWithPosition = (item, i) => ({ ...item, position: (i + 1) * 100 })

export interface MediaFieldProps {
  max?: number
  prefix: string // Lowercase model name e.g. 'post'
  name: string
  label?: string
  required?: boolean
  hideDelete?: boolean
  record: any // model item
  onChange: (items: object[] | null) => void
  dropzone?: Omit<DropzoneProps, 'accept'> & { accept?: string }
  list?: boolean // Set 'list' mode
  uploadCardProps?: { title?: string; subtitle?: string; padding?: string }
  uploadFieldImageProps?: any
}

const MediaField: React.FC<MediaFieldProps> = (props) => {
  const {
    uploadCardProps = {},
    list,
    onChange,
    max,
    name,
    label,
    required,
    record,
    prefix,
    dropzone,
    hideDelete,
  } = props
  const classes = useStyles(props)

  const initialValue = get(record, name)

  const [items, setItems] = useState(initialValue)

  const [isLoading, setIsLoading] = useState(false)

  const saveItems = (items) => {
    setItems(items)

    // Return null instead of empty array for Amplify AppSync GraphQL inputs
    if (Array.isArray(items) && items.length === 0) return onChange(null)

    return onChange(items)
  }

  const handleDrop = async (acceptedFiles) => {
    // Prevent user from uploading more than the max files
    if (max && acceptedFiles.length > max) return

    setIsLoading(true)

    // Send files to S3
    const promises = acceptedFiles.map(mapFileWithPrefix(prefix)).map(putS3File)
    const promised = await Promise.all(promises)

    setIsLoading(false)

    // Save files to DB
    const itemsToSave = items ? [...items, ...promised] : promised
    const nextItems = itemsToSave.map(mapItemWithPosition).map(removeTypename)
    return saveItems(nextItems)
  }

  const handleRemove = async (file) => {
    const { src } = file

    // Remove file from S3
    const onRemove = await Storage.remove(src)

    // Escape if unsucessful
    if (!onRemove) return

    // Save files to DB
    const nextItems = items
      .filter((item) => item.src !== file.src)
      .map(mapItemWithPosition)
      .map(removeTypename)
    return saveItems(nextItems)
  }

  const handleDrag = async (items) => {
    const nextItems = items.map(mapItemWithPosition).map(removeTypename)
    saveItems(nextItems)
  }

  const showDropzone = max && items ? items.length < max : true

  const [isRejected, setIsRejected] = useState(false)
  const handleClose = () => setIsRejected(false)
  const handleDropRejected = () => setIsRejected(true)

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop,
    onDropRejected: handleDropRejected,
    accept: 'image/*',
    minSize: 0,
    maxSize: MAX_FILE_SIZE,
    ...dropzone,
  })

  const maxSize = dropzone?.maxSize || MAX_FILE_SIZE

  const isVideo = /^video\//.test(String(dropzone?.accept))

  if (isLoading) return <Loading />

  return (
    <div className={clsx(classes.root, 'MuiUploadField-root')}>
      {label && showDropzone && (
        <FormLabel component="legend" className={classes.label} required={required}>
          {label}
        </FormLabel>
      )}
      <Box display="flex" overflow="auto" py={list ? 0 : 1.25} flexDirection={list ? 'column-reverse' : 'row'}>
        <DragProvider
          as={list ? List : 'div'}
          asProps={list && { dense: true, disablePadding: true }}
          items={items}
          onDrag={handleDrag}
          direction={list ? 'vertical' : 'horizontal'}
          className={classes.dragProvider}
        >
          {({ item }) => {
            const handleClear = (e) => {
              e.stopPropagation()
              return handleRemove(item)
            }

            if (list) {
              return (
                <ListItem key={item.src} className={classes.listItem}>
                  <ListItemAvatar>
                    <Avatar>
                      <DescriptionOutlinedIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={item.name} secondary={getFileSizeLimit(item.size)} />
                  {!hideDelete && (
                    <ListItemSecondaryAction>
                      <IconButton edge="end" aria-label="delete" onClick={handleClear}>
                        <ClearIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              )
            }

            return (
              <UploadFieldImage
                key={item.src}
                onClear={handleClear}
                as={isVideo ? S3Video : S3Image}
                ImageProps={{ src: item.src, as: Avatar }}
                hideDelete={hideDelete}
              />
            )
          }}
        </DragProvider>

        {showDropzone && (
          <div {...getRootProps()} className={classes.uploadContainer}>
            <input {...getInputProps()} />
            {list ? (
              <Card className={classes.uploadCard}>
                <CardActionArea>
                  <CloudUploadOutlinedIcon fontSize="large" color={isDragActive ? 'primary' : 'action'} />
                  <Box ml={2}>
                    {isDragActive ? (
                      <Typography variant="h5" color="primary">
                        Upload
                      </Typography>
                    ) : (
                      <>
                        <Typography variant="h5">{uploadCardProps.title || 'Select files'}</Typography>
                        <Typography color="textSecondary" variant="body1">
                          {uploadCardProps.subtitle || 'Drop files here or click to browse files'}
                        </Typography>
                      </>
                    )}
                  </Box>
                </CardActionArea>
              </Card>
            ) : (
              <Button className={classes.uploadButton} fullWidth={Boolean(list)} variant="outlined" color="primary">
                {isDragActive ? <CloudUploadOutlinedIcon /> : <AddIcon />}
              </Button>
            )}
          </div>
        )}

        <Snackbar
          open={isRejected}
          autoHideDuration={5000}
          onClose={handleClose}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        >
          <Alert onClose={handleClose} severity="error">
            {`Maximum image size of ${getFileSizeLimit(maxSize)} is exceeded.`}
          </Alert>
        </Snackbar>
      </Box>
    </div>
  )
}

export default MediaField
