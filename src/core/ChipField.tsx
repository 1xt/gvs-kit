import React from 'react'
import { Autocomplete } from '@material-ui/lab'
import TextField, { TextFieldProps } from './TextField'

interface ChipFieldProps extends TextFieldProps {
  options?: string[]
  value?: any
  onChange?: (data) => void
  onChangeSuccess?: () => void
  disabled?: boolean
}

const ChipField: React.FC<ChipFieldProps> = (props) => {
  const { options, onChange, value, onChangeSuccess, disabled, ...rest } = props

  const nextValue = value || []

  return (
    <Autocomplete
      freeSolo
      multiple
      options={options || []}
      renderInput={(params) => <TextField helperText={<span>Hit Enter &#8592; to Add</span>} {...params} {...rest} />}
      onChange={(e, data) => onChange?.(data)}
      value={nextValue}
      disabled={disabled}
    />
  )
}

export default ChipField
