import React, { useState } from 'react'
import clsx from 'clsx'
import { startCase, get } from 'lodash'
import { makeStyles } from '@material-ui/core/styles'
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core'
import InfiniteScroll from 'react-infinite-scroller'
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined'
import * as colors from '@material-ui/core/colors'
import TableEditBar from './TableEditBar'
import MoreButton from './MoreButton'

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    overflow: 'auto',
    padding: 0,
  },
  inner: {
    minWidth: 700,
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1),
  },
  actions: {
    minWidth: 200,
    marginLeft: 'auto',
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(1),
    },
    '& > * + *': {
      marginLeft: theme.spacing(1),
    },
  },
  empty: {
    '& > svg': {
      fontSize: 60,
      marginBottom: theme.spacing(1),
      color: colors.blueGrey[200],
    },
    '& > button': {
      marginTop: theme.spacing(2),
    },
  },
  recordsText: {
    marginLeft: theme.spacing(0.75),
  },
}))

const DataTable = (props) => {
  const {
    isNew,
    isEmpty,
    isHideCheckbox = false,
    renderRow,
    AddModal,
    name,
    columns,
    columnData,
    className,
    items = [],
    pagination,
    tableEditBarProps,
    moreActions,
    cardHeader,
    ...rest
  } = props
  const classes = useStyles()
  const [selectedItems, setSelectedItems]: any = useState([])

  const handleSelectAll = (event) => {
    const selectedItems = event.target.checked ? items : []

    setSelectedItems(selectedItems)
  }

  const handleSelectOne = (event, item) => {
    if (event.target.checked) return setSelectedItems(selectedItems.concat(item))
    return setSelectedItems(selectedItems.filter(({ id }) => id !== item.id))
  }

  if (isNew) {
    return (
      <Box className={classes.empty} mt={6} textAlign="center" maxWidth={280} mx="auto">
        <AddCircleOutlineOutlinedIcon />
        <Typography variant="h3" gutterBottom>
          Manage {startCase(name)}s
        </Typography>
        <Typography variant="body1" color="textSecondary" gutterBottom>
          Add {startCase(name)}s to get started. Setup your first {startCase(name)} to keep track of {startCase(name)}s
          easily.
        </Typography>
        <AddModal ButtonProps={{ fullWidth: true }} />
      </Box>
    )
  }

  const cardHeaderProps = moreActions ? { action: <MoreButton items={moreActions} /> } : {}

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Card>
        {cardHeader || (
          <CardHeader
            title={
              <Box display="flex" alignItems="center">
                All {startCase(name)}s
                <Typography color="textSecondary" variant="body2" className={classes.recordsText}>
                  {items.length} record(s) fetched
                </Typography>
              </Box>
            }
            {...cardHeaderProps}
          />
        )}
        <Divider />
        <CardContent className={classes.content}>
          <div className={classes.inner}>
            <Table role="table">
              <TableHead>
                <TableRow>
                  {!isHideCheckbox && (
                    <TableCell key="checkbox" padding="checkbox">
                      <Checkbox
                        checked={selectedItems.length === items.length}
                        color="primary"
                        indeterminate={selectedItems.length > 0 && selectedItems.length < items.length}
                        onChange={handleSelectAll}
                      />
                    </TableCell>
                  )}
                  {columns.map(({ name, label, TableCellProps }) => {
                    return (
                      <TableCell key={name} align={name === 'actions' ? 'right' : 'left'} {...TableCellProps}>
                        {label || startCase(name)}
                      </TableCell>
                    )
                  })}
                </TableRow>
              </TableHead>
              <InfiniteScroll
                element={TableBody}
                {...pagination}
                loader={
                  <tr className="loader" key={0}>
                    <td>Loading</td>
                  </tr>
                }
              >
                {items.map((item, index) => {
                  const isSelected = Boolean(selectedItems.find(({ id }) => id === item.id))
                  return typeof renderRow === 'function' ? (
                    renderRow(item, { ...columnData, index, isSelected })
                  ) : (
                    <TableRow hover key={item.id} selected={isSelected}>
                      {!isHideCheckbox && (
                        <TableCell key="checkbox" padding="checkbox">
                          <Checkbox
                            checked={isSelected}
                            color="primary"
                            onChange={(event) => handleSelectOne(event, item)}
                            value={isSelected}
                          />
                        </TableCell>
                      )}
                      {columns.map(({ name, render, TableCellProps }: any, i) => {
                        const children = render ? (
                          render(item, { ...columnData, index })
                        ) : (
                          <Typography variant="h6">{startCase(get(item, name))}</Typography>
                        )

                        if (i === 0) {
                          return (
                            <TableCell key={name} {...TableCellProps}>
                              {children}
                            </TableCell>
                          )
                        }

                        if (name === 'actions') {
                          return (
                            <TableCell key={name} align="right" {...TableCellProps}>
                              <div className={classes.actions}>{children}</div>
                            </TableCell>
                          )
                        }

                        return (
                          <TableCell key={name} {...TableCellProps}>
                            {children}
                          </TableCell>
                        )
                      })}
                    </TableRow>
                  )
                })}
              </InfiniteScroll>
            </Table>
          </div>
        </CardContent>
      </Card>
      <TableEditBar selected={selectedItems} {...tableEditBarProps} />
    </div>
  )
}

export default DataTable
