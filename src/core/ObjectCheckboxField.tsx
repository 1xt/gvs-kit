import React, { useState, useEffect } from 'react'
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormControlProps,
  FormGroup,
  FormHelperTextProps,
  FormLabel,
  Theme,
  FormHelperText,
  FormControlLabelProps,
} from '@material-ui/core'
import { ControllerProps } from 'react-hook-form'
import { makeStyles } from '@material-ui/core/styles'
import { getLabelFromName } from '../utils'

const useStyles = makeStyles((theme: Theme) => ({
  label: {
    marginLeft: 0,
    marginRight: 0,
  },
}))

interface CheckboxOption {
  id: string
}

interface ObjectCheckboxRenderProps {
  checkbox: JSX.Element
  option: CheckboxOption
  index: number
}

export interface ObjectCheckboxFieldProps {
  name: string
  options: CheckboxOption[]

  hideLabel?: boolean
  label?: string

  children?: (props: ObjectCheckboxRenderProps) => JSX.Element
  onChange: ControllerProps<any>['onChange']
  value: any

  wrapperComponent?: React.ElementType
  WrapperProps?: object
  FormControlProps?: FormControlProps
  FormHelperTextProps?: FormHelperTextProps
  FormControlLabelProps?: Partial<FormControlLabelProps>
  helperText?: string
}

/**
 * ObjectCheckboxField
 * Renders a MUI CheckboxGroupField with RHF Controller
 * Sets the value as an object instead of a string
 * And exposes a renderProp for more advanced checkboxes
 *
 * @param props
 * @constructor
 */
const ObjectCheckboxField: React.FC<ObjectCheckboxFieldProps> = (props) => {
  const {
    wrapperComponent: Wrapper = 'div',
    WrapperProps = {},
    children,
    hideLabel,
    options,
    name,
    label = getLabelFromName(name),
    onChange,
    value,
    FormControlProps = {},
    FormHelperTextProps = {},
    FormControlLabelProps = {},
    helperText,
  } = props
  const classes = useStyles(props)

  const [selectedOptions, setSelectedOptions] = useState(value)

  useEffect(() => {
    setSelectedOptions(value)
  }, [value])

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>, data: CheckboxOption) => {
    const { checked } = e.target
    const nextSelectedOptions = checked
      ? [...selectedOptions, data]
      : selectedOptions.filter(({ id }) => id !== data.id)
    setSelectedOptions(nextSelectedOptions)
    if (onChange) onChange(nextSelectedOptions)
  }

  const getChecked = (option) => selectedOptions.some((selectedOptions) => selectedOptions.id === option.id)

  return (
    <FormControl fullWidth {...FormControlProps}>
      {!hideLabel && <FormLabel component="legend">{label}</FormLabel>}
      <FormGroup>
        <Wrapper {...WrapperProps}>
          {options.map((option, index) => {
            const { id } = option

            const CheckboxElement = (
              <Checkbox
                name={name}
                value={option}
                onChange={(e) => handleChange(e, option)}
                checked={getChecked(option)}
              />
            )

            return (
              <FormControlLabel
                key={id}
                control={children ? children({ checkbox: CheckboxElement, option, index }) : CheckboxElement}
                label=""
                className={classes.label}
                {...FormControlLabelProps}
              />
            )
          })}
        </Wrapper>
      </FormGroup>
      {helperText && <FormHelperText {...FormHelperTextProps}>{helperText}</FormHelperText>}
    </FormControl>
  )
}

export default ObjectCheckboxField
