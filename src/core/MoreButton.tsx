import React, { useRef, useState } from 'react'
import { IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Tooltip } from '@material-ui/core'
import MoreIcon from '@material-ui/icons/MoreVert'

const MoreButton = (props) => {
  const { items, MenuProps, ...rest } = props
  const moreRef = useRef(null)
  const [openMenu, setOpenMenu] = useState(false)

  const handleMenuOpen = () => setOpenMenu(true)
  const handleMenuClose = () => setOpenMenu(false)

  return (
    <>
      <Tooltip title="More options">
        <IconButton {...rest} onClick={handleMenuOpen} ref={moreRef} size="small">
          <MoreIcon />
        </IconButton>
      </Tooltip>
      <Menu
        {...MenuProps}
        anchorEl={moreRef.current}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        elevation={1}
        onClose={handleMenuClose}
        open={openMenu}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        {items.map((item) => {
          const { title, icon: Icon, onClick } = item

          return (
            <MenuItem
              key={title}
              onClick={() => {
                onClick()
                handleMenuClose()
              }}
            >
              <ListItemIcon>{React.isValidElement(Icon) ? Icon : <Icon />}</ListItemIcon>
              <ListItemText primary={title} />
            </MenuItem>
          )
        })}
      </Menu>
    </>
  )
}

export default MoreButton
