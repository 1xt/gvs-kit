import React, { forwardRef, useEffect } from 'react'
import { startCase } from 'lodash'
import clsx from 'clsx'
import { TextField as MUITextField, TextFieldProps as MUITextFieldProps } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import getLabelFromName from '../utils/getLabelFromName'

const useStyles = makeStyles(() => ({
  hidden: {
    display: 'none',
  },
}))

export interface TextFieldProps extends Omit<MUITextFieldProps, 'variant'> {
  variant?: 'outlined' | 'filled' | 'standard' | any // Issue with MUI union for variant
  options?: any[]
  text?: boolean
  hidden?: boolean
  name: string
  hideLabel?: boolean

  // Injected props by RHF
  onChange?: (value: any) => void
  value?: any

  // Custom props
  errors?: Array<{ [key: string]: { message?: string; type?: string } }>
}

const TextField: React.FC<TextFieldProps> = forwardRef((props, ref) => {
  const {
    hidden,
    text,
    options,
    name,
    label,
    className,
    hideLabel,
    inputProps: injectedInputProps,
    errors,
    helperText,
    ...rest
  } = props
  const { value, onChange } = rest
  const classes = useStyles(props)

  const defaultProps: any = {
    name,
    variant: 'outlined',
    fullWidth: true,
    InputLabelProps: { shrink: value !== null || typeof value !== 'undefined' },
    inputProps: { 'aria-label': name, ...injectedInputProps },
    helperText,
  }

  // Pass ref directly into the MUI TextField.inputRef
  if (ref) defaultProps.inputRef = ref

  // Sugar for handling errors
  if (errors) {
    const error = errors[name]
    defaultProps.error = Boolean(error)
    defaultProps.helperText = error?.message || error?.type
  }

  if (!hidden && !hideLabel) defaultProps.label = label || getLabelFromName(name)

  // onChange of options, set defaultValue for Select fields to be first item in options
  useEffect(() => {
    if (options && (typeof value === 'undefined' || value === null) && onChange) {
      const isObject = typeof options[0] === 'object' && Boolean(options[0])
      onChange(isObject && options[0].hasOwnProperty('value') ? options[0].value : options[0])
    }
  }, [options])

  if (options) {
    defaultProps.select = true
    defaultProps.SelectProps = { native: true }

    return (
      <MUITextField {...defaultProps} {...rest}>
        {options.map((option) => {
          const labelKey = Object.keys(option).includes('title') ? 'title' : 'label'
          const shouldPrintLabel = typeof option === 'object' && option[labelKey]
          const isDisabled = option?.disabled || false

          return (
            <option disabled={isDisabled} value={option.value ?? option} key={option[labelKey] || option}>
              {shouldPrintLabel ? option[labelKey] : startCase(option)}
            </option>
          )
        })}
      </MUITextField>
    )
  }

  if (text) {
    defaultProps.type = 'textarea'
    defaultProps.multiline = true
    defaultProps.rows = 3
  }

  return <MUITextField {...defaultProps} className={clsx(className, hidden && classes.hidden)} {...rest} />
})

export default TextField
