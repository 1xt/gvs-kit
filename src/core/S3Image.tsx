import React, { useEffect, useState } from 'react'
import { Storage } from 'aws-amplify'
import Skeleton from '@material-ui/lab/Skeleton'

interface S3ImageProps {
  as: React.ElementType
  path?: string
  src?: string
}

const S3Image = (props) => {
  const { className, path, src, as: As, ...rest } = props
  const [nextSrc, setNextSrc] = useState('')

  const nextPath = src || path

  useEffect(() => {
    const fetchS3 = async () => {
      const data: any = await Storage.get(nextPath)
      if (data) setNextSrc(data)
    }

    if (nextPath) fetchS3()
    else setNextSrc('')
  }, [nextPath])

  const nextProps = {
    src: nextSrc,
    className,
    alt: '',
    ...rest,
  }

  if (!nextProps.src && nextProps.width && nextProps.height) {
    return <Skeleton variant="rect" width={nextProps.width} height={nextProps.height} />
  }

  if (As) return <As {...nextProps} />

  return <img alt="" {...nextProps} />
}

export default S3Image
