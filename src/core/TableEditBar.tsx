import React from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/styles'
import { Drawer, Grid, Typography, Button, Hidden, Theme } from '@material-ui/core'

interface Action {
  onClick: (item: any) => unknown
  icon: React.ElementType | React.ComponentType
  text: string
}

interface TableEditProps {
  selected: any[]
  className?: string
  actions?: Action[]
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  actions: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  buttonIcon: {
    marginRight: theme.spacing(1),
  },
}))

const TableEditBar: React.FC<TableEditProps> = (props) => {
  const { selected, className, actions = [], ...rest } = props
  const classes = useStyles()
  const open = selected.length > 0

  return (
    <Drawer
      anchor="bottom"
      open={open}
      // eslint-disable-next-line react/jsx-sort-props
      PaperProps={{ elevation: 1 }}
      variant="persistent"
    >
      <div {...rest} className={clsx(classes.root, className)}>
        <Grid alignItems="center" container spacing={2}>
          <Hidden smDown>
            <Grid item md={3}>
              <Typography color="textSecondary" variant="subtitle1">
                {selected.length} selected
              </Typography>
            </Grid>
          </Hidden>
          <Grid item md={6} xs={12}>
            <div className={classes.actions}>
              {actions.map(({ onClick, icon: Icon, text }) => (
                <Button key={text} onClick={() => onClick(selected)}>
                  {React.isValidElement(Icon) ? Icon : <Icon className={classes.buttonIcon} />}
                  {text}
                </Button>
              ))}
            </div>
          </Grid>
        </Grid>
      </div>
    </Drawer>
  )
}

export default TableEditBar
