import React from 'react'
import clsx from 'clsx'
import { sortBy } from 'lodash'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { Direction } from 'react-beautiful-dnd/src/types'
import { makeStyles } from '@material-ui/core/styles'
import { Box, BoxProps as MUIBoxProps } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  isDraggingOver: {},
  isDragging: {},
}))

export const setPositions = (items: any[]): any[] =>
  items.map(({ position, ...rest }, i) => ({ ...rest, position: (i + 1) * 100 }))

export const reorder = (items: any[], startIndex: number, endIndex: number): any[] => {
  const result = Array.from(items)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)
  return result
}

type DragItem = any

interface DragProviderRenderProps {
  item: DragItem
  index: number
}

interface DragProviderProps {
  items: DragItem[]
  onDrag: (items: DragItem[]) => void
  direction?: Direction
  children: (props: DragProviderRenderProps) => JSX.Element
  className?: string
  as?: React.ElementType
  asProps?: any
}

/**
 * @link https://codesandbox.io/s/mmrp44okvj?file=/index.js
 * @param props
 * @constructor
 */
const DragProvider: React.FC<DragProviderProps> = (props) => {
  const { asProps = {}, as: As = 'div', items, onDrag, direction = 'vertical', children, className } = props
  const classes = useStyles(props)

  const BoxProps: MUIBoxProps = {
    flexDirection: direction === 'vertical' ? 'column' : 'row',
  }

  const handleDragEnd = (e) => {
    const { source, destination } = e
    if (!destination) return
    const nextItems = reorder(items, source.index, destination.index)
    return onDrag(nextItems)
  }

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <Droppable droppableId="droppable" direction={direction}>
        {(provided, snapshot) => (
          <Box
            display="flex"
            {...BoxProps}
            // @ts-ignore
            ref={provided.innerRef}
            className={clsx(className, snapshot.isDraggingOver)}
          >
            {items &&
              sortBy(items).map((item, index) => {
                const key = String(item.position + index)
                return (
                  <Draggable index={index} draggableId={key} key={key}>
                    {(provided, snapshot) => (
                      <As
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className={clsx({ [classes.isDragging]: snapshot.isDragging })}
                        style={provided.draggableProps.style}
                        {...asProps}
                      >
                        {children({ item, index })}
                      </As>
                    )}
                  </Draggable>
                )
              })}
            {provided.placeholder}
          </Box>
        )}
      </Droppable>
    </DragDropContext>
  )
}

export default DragProvider
