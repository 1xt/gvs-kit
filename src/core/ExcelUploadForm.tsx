import React, { useCallback } from 'react'
import XLSX, { ParsingOptions } from 'xlsx'
import { useDropzone } from 'react-dropzone'
import DropzoneCard from './DropzoneCard'

export interface ExcelUploadData {
  excelData: any[] | null
  excelFile: File
}

interface ProductUploadFormProps {
  onUpload: (uploadData: ExcelUploadData) => void
  file?: File
  readOptions?: ParsingOptions
}

const ExcelUploadForm: React.FC<ProductUploadFormProps> = (props) => {
  const { onUpload, file, readOptions } = props

  const handleFile = (excelFile: File) => {
    const reader = new FileReader()
    const rABS = Boolean(reader.readAsBinaryString)

    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target?.result
      const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', ...readOptions })
      /* Get first worksheet */
      const wsname = wb.SheetNames[0]
      const ws = wb.Sheets[wsname]
      /* Convert array of arrays */
      const excelData = XLSX.utils.sheet_to_json(ws, { defval: null, blankrows: false })
      /* Update state */
      if (onUpload) onUpload({ excelData, excelFile })
    }

    const readType = rABS ? 'readAsBinaryString' : 'readAsArrayBuffer'

    reader[readType](excelFile)
  }

  const onDrop = useCallback((acceptedFiles) => {
    handleFile(acceptedFiles[0])
  }, [])

  const dropzone = useDropzone({ onDrop })

  return <DropzoneCard dropzone={dropzone} file={file} />
}

export default ExcelUploadForm
