import React from 'react'
import { startCase } from 'lodash'
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, RadioGroupProps } from '@material-ui/core'
import { getLabelFromName } from '../utils'

interface RadioFieldProps extends RadioGroupProps {
  options: string[]
  hideLabel?: boolean
  label?: string
  name: string
}

/**
 * RadioField
 * @param props
 * @constructor
 * @example
 * <Controller
 *   as={RadioField}
 *   control={control}
 *   name="status"
 *   options={['published', 'draft', 'archived']}
 *   hideLabel
 * />
 */
const RadioField: React.FC<RadioFieldProps> = (props) => {
  const { hideLabel, options, name, label, ...rest } = props

  return (
    <FormControl component="fieldset">
      {!hideLabel && <FormLabel component="legend">{label || getLabelFromName(name)}</FormLabel>}
      <RadioGroup name={name} {...rest}>
        {options.map((option) => {
          return <FormControlLabel key={option} value={option} control={<Radio />} label={startCase(option)} />
        })}
      </RadioGroup>
    </FormControl>
  )
}

export default RadioField
