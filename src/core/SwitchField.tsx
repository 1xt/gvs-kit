import React from 'react'
import clsx from 'clsx'
import { FormControl, FormControlLabel, FormGroup, Switch, SwitchProps, Theme } from '@material-ui/core'
import { FormProps } from 'react-hook-form'
import { makeStyles } from '@material-ui/core/styles'
import { getLabelFromName } from '../utils'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
  },
  formGroup: {
    height: '100%',
    justifyContent: 'center',
  },
  fullWidth: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginRight: theme.spacing(-1),
    marginLeft: 0,
  },
}))

interface SwitchFieldProps extends Omit<SwitchProps, 'defaultValue'> {
  name: string
  register: FormProps['register']
  defaultValue?: boolean
  label?: string
  fullWidth?: boolean
  hideLabel?: boolean
}

/**
 * SwitchField
 * @param props
 * @constructor
 * @example
 * <SwitchField
 *   name="isChecked"
 *   register={register}
 *   defaultValue={defaultValues.isChecked}
 * />
 */
const SwitchField: React.FC<SwitchFieldProps> = (props) => {
  const { hideLabel, fullWidth, name, label, register, defaultValue = false, size = 'medium' } = props
  const classes = useStyles(props)

  const [checked, setChecked] = React.useState(defaultValue)
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => setChecked(event.target.checked)

  const formControlLabelProps = {
    label: !hideLabel && (label || getLabelFromName(name)),
  }

  return (
    <FormControl component="fieldset" className={classes.root}>
      <FormGroup className={classes.formGroup}>
        <FormControlLabel
          className={clsx(fullWidth && classes.fullWidth)}
          control={<Switch name={name} inputRef={register} onChange={handleChange} checked={checked} size={size} />}
          {...formControlLabelProps}
        />
      </FormGroup>
    </FormControl>
  )
}

export default SwitchField
