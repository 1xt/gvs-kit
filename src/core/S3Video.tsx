import React, { forwardRef, VideoHTMLAttributes, useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Storage } from 'aws-amplify'
import clsx from 'clsx'

interface S3VideoProps extends VideoHTMLAttributes<HTMLVideoElement> {
  src: string
}

const useStyles = makeStyles(() => ({
  root: {
    objectFit: 'cover',
  },
}))

const S3Video = forwardRef<HTMLVideoElement, S3VideoProps>((props, ref) => {
  const { src: path, className, ...rest } = props
  const [src, setSrc] = useState<string | null>(null)

  const classes = useStyles()

  useEffect(() => {
    const fetchS3 = async () => {
      const url = await Storage.get(path)
      if (url && typeof url === 'string') setSrc(url)
    }

    fetchS3()
  }, [path])

  return (
    <video
      muted
      playsInline
      preload="metadata"
      ref={ref}
      src={`${src}#t=0.001`} // https://muffinman.io/hack-for-ios-safari-to-display-html-video-thumbnail/
      className={clsx(classes.root, className)}
      {...rest}
    />
  )
})

export default S3Video
