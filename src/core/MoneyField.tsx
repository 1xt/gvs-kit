import React from 'react'
import NumberFormat from 'react-number-format'
import TextField from './TextField'

interface NumberFormatCustomProps {
  inputRef: (instance: NumberFormat | null) => void
  onChange: (event: { target: { name: string; value: string } }) => void
  name: string
}

export function NumberFormatCustom(props: NumberFormatCustomProps) {
  const { inputRef, onChange, ...other } = props

  return (
    <NumberFormat
      decimalScale={2}
      fixedDecimalScale
      thousandSeparator
      isNumericString
      prefix="$"
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        })
      }}
    />
  )
}

const MoneyField = (props) => {
  const { onChange, value, name, ...rest } = props

  const handleChange = (e) => onChange(Number(e.target.value))

  return (
    <TextField
      value={value}
      onChange={handleChange}
      name={name}
      InputProps={{
        inputComponent: NumberFormatCustom as any,
      }}
      {...rest}
    />
  )
}

export default MoneyField
