import React from 'react'
import { CircularProgress, CircularProgressProps, Box, BoxProps } from '@material-ui/core'

interface LoadingProps extends BoxProps {
  CircularProgressProps?: CircularProgressProps
}

const Loading: React.FC<LoadingProps> = (props) => {
  const { CircularProgressProps, ...rest } = props
  return (
    <Box display="flex" alignItems="center" justifyContent="center" height="100%" {...rest}>
      <CircularProgress {...CircularProgressProps} />
    </Box>
  )
}

export default Loading
