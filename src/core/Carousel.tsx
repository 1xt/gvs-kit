import React from 'react'
import NukaCarousel, { CarouselProps } from 'nuka-carousel'
import { IconButton } from '@material-ui/core'
import ChevronLeftOutlinedIcon from '@material-ui/icons/ChevronLeftOutlined'
import ChevronRightOutlinedIcon from '@material-ui/icons/ChevronRightOutlined'
import * as colors from '@material-ui/core/colors'

const carouselProps = {
  slidesToShow: 3,
  slidesToScroll: 3,
  cellSpacing: 8,
  enableKeyboardControls: true,
  renderCenterLeftControls: ({ previousSlide }) => (
    <IconButton onClick={previousSlide}>
      <ChevronLeftOutlinedIcon />
    </IconButton>
  ),
  renderCenterRightControls: ({ nextSlide }) => (
    <IconButton onClick={nextSlide}>
      <ChevronRightOutlinedIcon />
    </IconButton>
  ),
  defaultControlsConfig: {
    pagingDotsStyle: {
      fill: colors.blueGrey[600],
    },
  },
}

const Carousel: React.FC<CarouselProps> = (props) => {
  const { children, ...rest } = props

  return (
    <NukaCarousel {...carouselProps} {...rest}>
      {children}
    </NukaCarousel>
  )
}

export default Carousel
