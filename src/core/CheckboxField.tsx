import React from 'react'
import { startCase } from 'lodash'
import { Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel } from '@material-ui/core'
import { FormProps } from 'react-hook-form'
import { getLabelFromName } from '../utils'

interface CheckboxFieldProps {
  name: string
  register: FormProps['register']
  defaultValue: string[]
  options: string[]
  hideLabel?: boolean
  label?: string
}

/**
 * CheckboxGroupField
 * @param props
 * @constructor
 * @example
 * <CheckboxField
 *   defaultValue={defaultValues.statuses}
 *   register={register}
 *   name="statuses"
 *   options={['published', 'draft', 'archived']}
 *   hideLabel
 * />
 */
const CheckboxField: React.FC<CheckboxFieldProps> = (props) => {
  const { hideLabel, options, name, label = getLabelFromName(name), register, defaultValue = [] } = props

  const initialState = options.reduce((acc, option) => {
    return { ...acc, [option]: defaultValue.includes(option) }
  }, {})

  const [state, setState] = React.useState(initialState)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    return setState({ ...state, [event.target.value]: event.target.checked })
  }

  return (
    <FormControl component="fieldset">
      {!hideLabel && <FormLabel component="legend">{label}</FormLabel>}
      <FormGroup>
        {options.map((option, i) => {
          return (
            <FormControlLabel
              key={option}
              control={
                <Checkbox
                  value={option}
                  name={name}
                  inputRef={register}
                  onChange={handleChange}
                  checked={state[option]}
                />
              }
              label={startCase(option)}
            />
          )
        })}
      </FormGroup>
    </FormControl>
  )
}

export default CheckboxField
