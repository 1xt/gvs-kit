import React from 'react'
import MUIAutocomplete, { AutocompleteProps as MUIAutocompleteProps } from '@material-ui/lab/Autocomplete'
import { BaseTextFieldProps, CircularProgress, TextField } from '@material-ui/core'
import { startCase } from 'lodash'
import { TextFieldProps } from './TextField'
import { OptionType, getGraphQLConnectionFieldValue } from '../utils'

export interface AutocompleteFieldProps extends BaseTextFieldProps {
  multiple?: boolean
  disableCloseOnSelect?: boolean
  filter?: object
  TextFieldProps?: TextFieldProps
}

export interface AutocompleteProps
  extends BaseTextFieldProps,
    Omit<MUIAutocompleteProps<any>, 'options' | 'renderInput' | 'ref' | 'classes' | 'color' | 'autoComplete'> {
  // Controller props
  name?: string
  onChange?: any
  value?: any
  // Autocomplete props
  multiple?: boolean
  options: OptionType[]
  pk: string
  TextFieldProps?: TextFieldProps
}

const Autocomplete: React.FC<AutocompleteProps> = (props) => {
  const {
    disabled,
    loading,
    pk,
    multiple,
    options,
    name,
    label,
    onChange,
    value,
    required,
    TextFieldProps = {},
    ...rest
  } = props

  const nextValue: any = getGraphQLConnectionFieldValue(pk, options, value, multiple)
  const nextOptions = loading ? [] : options

  return (
    // Fix issue with multiple prop on Autocomplete
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    <MUIAutocomplete
      {...rest}
      disabled={disabled || loading}
      multiple={multiple}
      filterSelectedOptions={multiple}
      onChange={(e, data) => onChange(data)}
      options={nextOptions || []}
      value={nextValue}
      getOptionLabel={(option: { title?: string | null }) => option.title || ''}
      getOptionSelected={(option, data) => option[pk] === data[pk]}
      renderInput={(params) => (
        <TextField
          {...params}
          name={name}
          required={required}
          label={label || startCase(name)}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading && <CircularProgress color="inherit" size={20} />}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
          variant="outlined"
          fullWidth
          {...TextFieldProps}
        />
      )}
    />
  )
}

export default Autocomplete
