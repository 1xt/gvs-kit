import React from 'react'
import { startCase } from 'lodash'
import { DateTimePicker } from '@material-ui/pickers'

const DateTimeField = (props) => {
  const { onChange, value, name, label, required, ...rest } = props

  const handleChange = (date) => onChange(date.toISOString())

  return (
    <DateTimePicker
      {...rest}
      fullWidth
      inputVariant="outlined"
      label={label || startCase(name)}
      name={name}
      onChange={handleChange}
      value={value}
      required={required}
      // https://github.com/mui-org/material-ui-pickers/issues/610
      // Override DateTimePicker's readonly attribute on input element to allow HTML required validation
      inputProps={{ readOnly: !required }}
    />
  )
}

export default DateTimeField
