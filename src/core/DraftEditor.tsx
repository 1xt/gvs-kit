import React, { useState } from 'react'
import BraftEditor, { ControlType } from 'braft-editor'
import clsx from 'clsx'
import startCase from 'lodash/startCase'
import { Theme, InputLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import * as colors from '@material-ui/core/colors'

const CLASS_NAME = 'MuiEditor-root'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'relative',
  },
  label: {
    position: 'absolute',
    top: theme.spacing(-1),
    left: theme.spacing(1),
    zIndex: 1,
    padding: theme.spacing(0, 1),
    backgroundColor: theme.palette.common.white,
  },
  editor: {
    overflow: 'hidden',
    '& .bf-controlbar': {
      border: `1px solid ${colors.grey[100]}`,
      boxShadow: 'none',
    },
    '& .bf-content': {
      height: 150,
      border: `1px solid ${colors.grey[100]}`,
    },
  },
}))

// @link https://www.yuque.com/braft-editor/be/gz44tn
const controls = [
  'undo',
  'redo',
  'separator',
  'font-size',
  'separator',
  'bold',
  'italic',
  'separator',
  'separator',
  'link',
  'separator',
  'separator',
  'fullscreen',
] as ControlType[]

const DraftEditor = (props) => {
  const { name, label, className, onChange, ...rest } = props
  const { value } = rest
  const classes = useStyles(props)

  const [editorState, setEditorState] = useState(BraftEditor.createEditorState(value))

  const handleChange = (editorState) => {
    setEditorState(editorState)
    onChange(editorState)
  }

  return (
    <div className={clsx(CLASS_NAME, classes.root)}>
      <InputLabel className={classes.label} shrink>
        {label || startCase(name)}
      </InputLabel>
      <div className={classes.editor}>
        <BraftEditor controls={controls} language="en" value={editorState} onChange={handleChange} />
      </div>
    </div>
  )
}

export default DraftEditor
