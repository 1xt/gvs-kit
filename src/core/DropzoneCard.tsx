import React from 'react'
import { Card, CardActionArea, CardContent, Typography, Theme } from '@material-ui/core'
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined'
import { makeStyles } from '@material-ui/core/styles'
import { DropzoneState } from 'react-dropzone'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    textAlign: 'center',
  },
}))

export interface DropzoneCardProps {
  dropzone: DropzoneState
  file?: File
}

const DropzoneCard: React.FC<DropzoneCardProps> = (props) => {
  const { dropzone, file } = props
  const { isDragActive, getRootProps, getInputProps } = dropzone

  const classes = useStyles(props)

  const UploadIcon = isDragActive ? CloudUploadIcon : CloudUploadOutlinedIcon

  const card = (
    <Card>
      <CardActionArea>
        <CardContent>
          {file ? <DescriptionOutlinedIcon color="primary" /> : <UploadIcon />}
          <Typography gutterBottom variant="h5" component="h2">
            {file ? `${file.name}` : 'Upload'}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {file
              ? 'File uploaded successfully. Drag & drop or click here to upload another file'
              : 'Drag & drop or click here to upload'}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )

  return (
    <div className={classes.root} {...getRootProps()}>
      <input {...getInputProps()} />
      {card}
    </div>
  )
}

export default DropzoneCard
